﻿using IrrParser2.Data;
using IrrParser2.Download.Slando;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace IrrParser2.Parse.SlandoUa.Helpfull
{
   abstract class SlandoCategory
    {
        private string regionContent;
        private string country;
        private JavaScriptSerializer jss;

        private static readonly string postPatern = "{\"id\":1,\"jsonrpc\":\"2.0\",\"method\":\"subregions_getAllFromRegion\",\"params\":[idScalar]}";
        private static readonly string replacePattern = "idScalar";
       public SlandoCategory(string country)
        {
            this.country = country;
            jss = new JavaScriptSerializer();
        }


        public string GetRegionName(int regionId) 
        {
            if (regionContent == null) 
            {
                regionContent = Db.SelectSlandoJsRegion(country);
            }

           return GetRegionByContent(regionId, regionContent);
        }

       
        public string GetSubRegionName(int subRegionId) 
        {
            string postText = postPatern.Replace(replacePattern, subRegionId.ToString());
            string content = SectionDownSlando.UploadPost(postText);
          return  GetRegionByContent(subRegionId, content);
        }
        private string GetRegionByContent(int idReg, string content)
        {

            var dataAll = jss.Deserialize<SlandoJsonCategory>(content);
            string regionName = dataAll.result.Values
                .Where(x => x.id == idReg)
                .DefaultIfEmpty(new ValueJson { id = -1, name = "регион не найден" })
                .First()
                .name;
            return regionName;
        }
        public string GetCategoryName(int categoryId);

        public string GetSubCategoryName(int subcategory);
       

    }
}
